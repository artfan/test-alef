'use strict';
let menuOpen = document.querySelector('.open-mobile-menu'),
menuClose = document.querySelector('.close-mobile-menu'),
menu = document.querySelector('.mobile-menu'),
switchBtns = document.querySelectorAll('.switch-prev__btn'),
previewBig = document.querySelector('.preview__big'),
sizeBtn = document.querySelector('.size__btn'),
sizeHelp = document.querySelector('.size__tooltip'),
quantPlus = document.querySelector('.quant-plus'),
quantMinus = document.querySelector('.quant-minus'),
quantVal = document.querySelector('.quant-val'),
addCart = document.querySelector('.add-to-cart'),
cartItems = document.querySelector('.cart-items'),
addFav = document.querySelector('.add-to-favorite'),
favorite = document.querySelector('.favorite'),
modal = document.querySelector('.modal'),
modalContent = document.querySelector('.modal__content'),
header = document.querySelector('.header'),
itemName = document.querySelector('.info__title'),
emailInput = document.querySelector('.email-input'),
errorInput = document.querySelector('.subscribe-form__error');

function mobileMenu(){
    menu.classList.toggle('mobile-menu--open');
}
menuOpen.addEventListener('click', mobileMenu);
menuClose.addEventListener('click', mobileMenu);

switchBtns.forEach(switchBtn => {
    switchBtn.addEventListener('click', switchPreview)
});

function switchPreview(){
    previewBig.childNodes[0].attributes.srcset.value = this.previousElementSibling.children[0].attributes.srcset.value,
    previewBig.childNodes[1].attributes.src.value = this.previousElementSibling.children[1].attributes.src.value;
}

sizeBtn.addEventListener('click', function(){
    sizeHelp.classList.toggle('size__tooltip--show');
    setTimeout(hideTooltip, 2000);   
})

function hideTooltip(){
    sizeHelp.classList.toggle('size__tooltip--show')
}

quantPlus.addEventListener('click', quantMore);
quantMinus.addEventListener('click', quantLess);

function quantMore(){
    quantVal.value = parseInt(quantVal.value) + 1;
}
function quantLess(){
    if(!(parseInt(quantVal.value) == 1)){
        quantVal.value = parseInt(quantVal.value) - 1;
    }
}
addCart.addEventListener('click', function(){
    modalShow();
    if (parseInt(quantVal.value) > 1){
        modalContent.innerHTML = 'Товар ' + itemName.innerHTML + ' в количестве ' + quantVal.value +' единиц добавлен в корзину';
    } else {
        modalContent.innerHTML = 'Товар "' + itemName.innerHTML + '" в количестве ' + quantVal.value +' единицы добавлен в корзину';
    }
    cartItems.classList.add('head-nav__cart-items--added')
    cartItems.innerHTML = quantVal.value;
});
addFav.addEventListener('click', function(){
    modalShow();
    if (!favorite.classList.contains('head-nav__link--favorite')){
        modalContent.innerHTML = 'Товар "' + itemName.innerHTML + '" добавлен в избранное';
    } else {
        modalContent.innerHTML = 'Товар ' + itemName.innerHTML + ' удален из избранного';
        
    }
    favorite.classList.toggle('head-nav__link--favorite')
});
function modalHide(){
    modal.classList.remove('modal--show')
}
function modalShow(){
    modal.classList.add('modal--show');   
    setTimeout(modalHide, 2000);   
}
let now = 0;
window.addEventListener('scroll', function(){
    if ((window.scrollY > header.offsetHeight) && (now < window.scrollY)){
        header.classList.add('header--hide');
    } else {
        header.classList.remove('header--hide');
    }
    now = window.scrollY;
})

emailInput.addEventListener('change', function(){
    console.log(emailInput.value);
    let mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(!emailInput.value.match(mailformat)){
        errorInput.classList.add('subscribe-form__error--show')
    }
    else{
        errorInput.classList.remove('subscribe-form__error--show')
    }


})